package veovismuaddib.linking_tweaks.listeners;

import com.xcompwiz.mystcraft.api.event.StarFissureLinkEvent;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.DamageSource;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.common.DimensionManager;
import net.minecraftforge.event.entity.living.LivingFallEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import veovismuaddib.linking_tweaks.ModConfig;
import veovismuaddib.linking_tweaks.ModMain;

public class StarFissureDropListener {
    @SubscribeEvent
    public void onStarFissureLink(StarFissureLinkEvent e) {
        if(!ModConfig.fissure_drop.enabled) return;

        StarFissureSavedData data = StarFissureSavedData.get(e.worldObj);
        BlockPos spawn = DimensionManager.getProvider(e.info.getDimensionUID()).getRandomizedSpawnPoint();

        e.info.setSpawn(spawn.up(ModConfig.fissure_drop.height));
        if(e.entity instanceof EntityLivingBase) {
            data.addSurvivingEntity(e.entity);
            ModMain.logger.info("Preparing to save " + e.entity.getPersistentID().toString() + " from a star fissure fall.");
        }
    }

    @SubscribeEvent
    public void onStarFissureFall(LivingFallEvent e) {
        EntityLivingBase entity = e.getEntityLiving();
        StarFissureSavedData data = StarFissureSavedData.get(e.getEntity().world);

        if(data.containsSurvivingEntity(entity)) {
            if(!e.getEntity().getEntityWorld().isRemote) {
                boolean saved = true;

                switch(ModConfig.fissure_drop.fall_safety_type) {
                    case hearts_left:
                        heartsLeftHandler(e, entity);
                        break;
                    case hearts_taken:
                        heartsTakenHandler(e, entity);
                        break;
                    case no_damage:
                        noDamageHandler(e);
                        break;
                    case normal_damage:
                        saved = false;
                        break;
                }

                if(saved) {
                    ModMain.logger.info("Saved " + entity.getPersistentID().toString() + " from a fall of " + e.getDistance() + "m.");
                }

                data.removeSurvivingEntity(entity);
            }
        }
    }

    private void heartsLeftHandler(LivingFallEvent e, EntityLivingBase entity) {
        float entityHealth = entity.getHealth();
        float minimumHealth = Math.min(entityHealth, ModConfig.fissure_drop.fall_safety_hearts);
        entity.attackEntityFrom(DamageSource.FALL, Math.max(0, entityHealth - minimumHealth));
        e.setDamageMultiplier(0.0f);
    }

    private void heartsTakenHandler(LivingFallEvent e, EntityLivingBase entity) {
        entity.attackEntityFrom(DamageSource.FALL, ModConfig.fissure_drop.fall_safety_hearts);
        e.setDamageMultiplier(0.0f);
    }

    private void noDamageHandler(LivingFallEvent e) {
        e.setDamageMultiplier(0.0f);
    }
}
