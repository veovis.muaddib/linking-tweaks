package veovismuaddib.linking_tweaks.listeners;

import net.minecraft.entity.Entity;
import net.minecraft.nbt.*;
import net.minecraft.world.World;
import net.minecraft.world.storage.MapStorage;
import net.minecraft.world.storage.WorldSavedData;
import net.minecraftforge.common.util.Constants;
import veovismuaddib.linking_tweaks.ModMain;

import java.util.*;

public class StarFissureSavedData extends WorldSavedData {
    private static final String DATA_NAME = ModMain.MODID + "_star_fissures";
    private static final String LIST_NAME = "ProtectedPlayers";

    private static final Set<UUID> survivingEntities = new TreeSet<>();
    // Required constructors
    public StarFissureSavedData() {
      super(DATA_NAME);
    }

    public StarFissureSavedData(String s) {
      super(s);
    }

    public static StarFissureSavedData get(World world) {
        MapStorage storage = world.getMapStorage();
        StarFissureSavedData instance = null;

        try {
            instance = (StarFissureSavedData)storage.getOrLoadData(StarFissureSavedData.class, DATA_NAME);
        } finally {
            if(instance == null) {
                instance = new StarFissureSavedData();
                storage.setData(DATA_NAME, instance);
            }
        }

        return instance;
    }

    public void addSurvivingEntity(Entity e) {
        survivingEntities.add(e.getPersistentID());
        markDirty();
    }

    public void removeSurvivingEntity(Entity e) {
        survivingEntities.remove(e.getPersistentID());
        markDirty();
    }

    public boolean containsSurvivingEntity(Entity e) {
        return survivingEntities.contains(e.getPersistentID());
    }

    @Override
    public void readFromNBT(NBTTagCompound nbt) {
        ModMain.logger.info("Reading from NBT");
        NBTTagList survivingEntitiesNbt = nbt.getTagList(LIST_NAME, Constants.NBT.TAG_STRING);
        for(NBTBase nbtId: survivingEntitiesNbt) {
            try {
                String idString = ((NBTTagString) nbtId).getString();
                UUID id = UUID.fromString(idString);
                survivingEntities.add(id);
            } catch(Exception e) {
                ModMain.logger.error("Failed to parse surviving entities from NBT");
            }
        }
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound nbt) {
        ModMain.logger.info("Writing to NBT");
        NBTTagList survivingEntitiesNbt = nbt.getTagList(LIST_NAME, Constants.NBT.TAG_STRING);
        for(UUID id: survivingEntities) {
            survivingEntitiesNbt.appendTag(new NBTTagString(id.toString()));
        }
        nbt.setTag("uuids", survivingEntitiesNbt);
        return nbt;
    }
}
