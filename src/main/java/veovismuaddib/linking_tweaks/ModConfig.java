package veovismuaddib.linking_tweaks;

import net.minecraftforge.common.config.Config;

@Config(modid = ModMain.MODID, name = "mystcraft/linking_tweaks", category = "")
public class ModConfig {
    @Config.Comment("Dropping through the Star Fissure causes entities to fall into the Overworld")
    public static StarFissureConfig fissure_drop = new StarFissureConfig();

    public static class StarFissureConfig {
        public boolean enabled = true;

        @Config.RangeInt(min= 1, max = 10000)
        @Config.Comment("The offset in meters from the spawn point where entities will link to")
        public int height = 512;

        @Config.Comment({
            "Whether to save entities that fall through the fissure from taking full damage. This applies to the first landing only.",
            "This option does not currently detect if players have aerial control or flight capabilities.",
            "Options that deal with heart amounts are in half-hearts and are configured by fall_safety_hearts.",
            "  * normal_damage: Entities will take as much damage as they would otherwise have taken from the fall",
            "  * hearts_left: Entities will survive the fall with x half-hearts remaining, regardless of fall distance (including falls that would otherwise not hurt)",
            "  * hearts_taken: Entities will take x damage, regardless of fall distance (including falls that would otherwise not hurt)",
            "  * no_damage: Entities will take no damage from the fall after linking through a star fissure."
        })
        public FALL_SAFETY_TYPES fall_safety_type = FALL_SAFETY_TYPES.hearts_left;

        @Config.Comment({
            "Configure fall safety damage, in hearts.",
            "Behavior is dependent on fall_safety_type",
            "  * hearts_left: The number of half-hearts entities will keep after landing.  If this is above 0, the entity will survive the fall.",
            "  * hearts_taken: The number of half-hearts of damage entities will take upon landing.  If this is 0, the entity will survive the fall"
        })
        @Config.RangeInt(min = 0, max = 100)
        public int fall_safety_hearts = 1;

        public enum FALL_SAFETY_TYPES {
            normal_damage,
            hearts_left,
            hearts_taken,
            no_damage,
        }
    }
}
