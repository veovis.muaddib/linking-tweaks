# 1.0.0.0

* Drop players from above overworld spawn after linking through a star fissure (Default enabled, 512m)
* Prevent/reduce damage from the first landing after linking through a star fissure (Default 1/2 heart remaining)
